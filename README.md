
## Influence of peer reviewed journals in the literature of conservation policy and practice

This project looks at the influence of scientific literature in the literature of conservation policy and practice. It does so by examining the peer reviewed content in primary documents, reports and publications produced by major environmental organisations. 

The work focuses on changes in the use of peer reviewed literature over time, and takes a closer look at the different types of academic areas that have been cited.

The project is currently in the write-up stage. 

- [Project code](https://gitlab.com/guizar/public-conservation-reports/tree/master/code)
- [Charts and figures](https://gitlab.com/guizar/public-conservation-reports/tree/master/png) (additional description to be provided in the paper)