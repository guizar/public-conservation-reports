# ---
# Parse journal subjects
# ---

import pandas as pd
import unicodedata
import os
import re
from StringIO import StringIO


# --- Define WDs ----
# define home
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")

# define working dirs
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wddata = os.path.join(proj,"data/")

# load data
file = "MASTER-JOURNAL-SUBJECT-ISSN-plus5-data.csv"
journals_table = pd.read_csv(os.path.join(wdtables,file),encoding = 'utf-8')

# ---- DEFINE FUNCTIONS  ---- #

def rem_noise(subject): # remove strings "(all)"
    if subject is not None:
        return re.sub(" ?\\(.*?\\)",'',subject)

def split_colon(subject): # Identify strings joined by a comma without a blank space, these are the marks between categories combinations (e.g. Nature and Landscape Conservation,Ecology, Evolution, Behavior and Systematics)    
    if subject is not None:
        return re.sub(r'\b,\b',';',subject)

def trim_semicolon_space(subject): 
    if subject is not None:
        return re.sub(r' ?; ?',';',subject)


#re.findall(r'\b,\b', subject) // TO JUST SEARCH


# Run FUNS
journals_table.SUBJECT = journals_table.SUBJECT.apply(lambda x: rem_noise(x))
pd.Series.tolist(journals_table.SUBJECT)

journals_table.SUBJECT = journals_table.SUBJECT.apply(lambda x: split_colon(x))
pd.Series.tolist(journals_table.SUBJECT)[0:5]

journals_table.SUBJECT = journals_table.SUBJECT.apply(lambda x: trim_semicolon_space(x))
pd.Series.tolist(journals_table.SUBJECT)[310:321]

journals_table.SUBJECT = journals_table.SUBJECT.str.strip()
pd.Series.tolist(journals_table.SUBJECT)


# ---- SAVE DATA OBJECT----

# DATA
# journals_table.to_csv(os.path.join(wddata,file),index=False, header=True, encoding="utf-8")
journals_table.to_csv(os.path.join(wdtables,file),index=False, header=True, encoding="utf-8")



