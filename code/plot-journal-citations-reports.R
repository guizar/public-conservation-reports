#----
# Journal cites 
# x: TOTAL CITATIONS
# Y: CITING REPORTS
# ----
library(tidyverse)
library(dplyr)
library(tidyr)
library(ggplot2)

# ---- load
journals =  read.csv(file ="data/journals-summary-scopus-extended.csv",stringsAsFactors = F)

# --- PLOT
gg = ggplot(journals, aes(x=N,y=PUBLICATIONS,size=N))
gg = gg + geom_point()
gg = gg +  geom_text(aes(label=JOURNAL), 
                     data = journals %>% top_n(., 11, N),
                     colour = "Black",
                     vjust = -1.2, hjust=1,size=3)
gg = gg + xlab("Total journal cites")  + ylab("Total citing reports")+ ggtitle("Journal citations")
gg

ggsave("png/journal-cites-dotplot.png",gg,scale = 2)
