library(dplyr)
library(tidyr)
library(lazyeval)

# ----CALCULATE JOURNAL COUNT BY ACADEMIC SUBJECT ---
# In this process we will get the number of journals "covered" in each academic field. I.e. - if a journal is related to 3 academic fields, each of these will recive a +1 count.

journals_table =  read.csv("data/journals-summary-scopus-extended.csv",strip.white = T,stringsAsFactors = F)

asjc = read.csv("scopus-sources-052016/ASJC-code-list.csv", strip.white = T, stringsAsFactors = F) %>% select(c(Description, Code))


# what's the max number of items in a record?
list = strsplit(journals_table$ASJC_CODES,split = ";") %>% lapply(.,FUN = length)
ncols = max(unlist(list[1:length(list)]))
ncols

# ---- Select columns, separate by semicollon and gather in a long format ----
subjects = 
  journals_table %>%
  separate(.,ASJC_CODES, 
           into = paste0("s",seq(1:ncols)),sep = ";",
           remove = F) %>%
  select(c(JOURNAL,s1:s6)) %>%
  gather(.,"s","VALUE",s1:s6) %>% 
  select(.,-s) %>%
  filter(VALUE !="") %>%
  filter(!is.na(VALUE)) 

subjects$VALUE = as.integer(subjects$VALUE)


# ---- Generate contingency table ----
subjects_ctable = table(subjects)

# to data frame
subjects_ctable = as.data.frame(subjects_ctable)

# wrangle contingency table
subjects_ctable =
  subjects_ctable %>% 
  spread(.,key = VALUE,value = Freq) %>% # SPREAD FREQS
  select(.,-JOURNAL) %>% 
  
  # Sum subjects counts
  summarise_each(funs(sum)) %>%
  gather(.,SUBJECT,JOURNAL_COUNT) %>%
  
  # Change from character to integer
  mutate(SUBJECT = as.integer(SUBJECT)) %>%
  
  # Add count percentages
  mutate(
    PERCENT = round((JOURNAL_COUNT/sum(JOURNAL_COUNT)) * 100,digits = 2)) %>%
  arrange(desc(PERCENT)) %>% 

  # Join codes
  left_join(.,asjc, by =c("SUBJECT" = "Code"))%>%

  # save
  write.csv(.,file = "tables/by-academic-subjects-journal-count.csv",row.names = F)




