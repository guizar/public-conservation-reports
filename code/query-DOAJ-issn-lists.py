# ---
# query DOAJ 
# ---
import pandas as pd
import unicodedata
import os
import re
import shelve
import urllib2

# --- Define WDs ----
# define home
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")

# define working dirs
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wddata = os.path.join(proj,"data/")

# load data
file = "MASTER-JOURNAL-SUBJECT-ISSN-plus5-data.csv"
journals_table = pd.read_csv(os.path.join(wdtables,file),encoding = 'utf-8')

# --- DEFINE FUNCTIONS ----
# PARSE DOAJ query
def parse_results(results):
    '''
    Analyses the "results" object resulting from the DOAJ ISSN search
    '''
    if not results: # if empty continue
        active =  ""
        oa_start_year =  ""

    else: # parse data and get active and oa_start values
        try: # try if the following fields are avaliable
            bibjson = results[0]['bibjson']
            bibjson['active']
            bibjson['oa_start']['year']
        except:
            active =  "Not Avaliable"
            oa_start_year =  "Not Avaliable"
        else:
            active =  bibjson['active']
            oa_start_year =  bibjson['oa_start']['year']

    return(active,oa_start_year)

# DOAJ query
def doaj_search_issn(issn):
    '''
    Takes an ISSN which is used to query DOAJ
    Returns a Panda data frame with 3 columns
    '''
    issn = issn
    end_url="?page=1&pageSize=1"
    url = "https://doaj.org/api/v1/search/journals/issn%3A" + str(issn) + end_url
    request = urllib2.Request(url)
    
    try:
        response = urllib2.urlopen(request, timeout=3)
        data = json.load(response)
        results = data['results']
    except: return None

    active, oa_start_year =  parse_results(results)

    return active, oa_start_year

# QUERY FROM LISTS
def query_from_issn_lists(issnBatch, issnIx):
    querylist = []
    for e in range(0, len(issnBatch)):
        if str(issnBatch[e]) != 'nan':
            active, oa_start_year = doaj_search_issn(issnBatch[e])
        queryResults = {'Ix':issnIx[e] , 'ISSN': issnBatch[e], 'Active': active, 'OA_YR':oa_start_year}
        querylist.append(queryResults)
    return querylist

# ----

# prepare functions to generate batches (lists)
batchesOf = len(journals_table) / 4 # looks like 4 is a sensible limit in DOAJ
def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i+n]
        
# generate batches of ISSNs
issnBatches = chunks(pd.Series.tolist(journals_table.ISSN),batchesOf)
issnBatch1 = issnBatches.next()
issnBatch2 = issnBatches.next()
issnBatch3 = issnBatches.next()
issnBatch4 = issnBatches.next()

# batches of IX
issnIx = chunks(range(0,len(journals_table)),batchesOf)
issnIx1 = issnIx.next()
issnIx2 = issnIx.next()
issnIx3 = issnIx.next()
issnIx4 = issnIx.next()


# ---- RUN FUNS --- 
# QUERY DOAJ
# run one by one, and allow a few minutes between each attempt 
# to prevent the server from stopping the process...
issnQuery1 = query_from_issn_lists(issnBatch1,issnIx1)
issnQuery2 = query_from_issn_lists(issnBatch2,issnIx2)
issnQuery3 = query_from_issn_lists(issnBatch3,issnIx3)
issnQuery4 = query_from_issn_lists(issnBatch4,issnIx4)

# to Pandas
query1 = pd.DataFrame(issnQuery1)
query2 = pd.DataFrame(issnQuery2)
query3 = pd.DataFrame(issnQuery3)
query4 = pd.DataFrame(issnQuery4)

# merge tables
frames = [query1, query2, query3, query4]
doajSearch = pd.concat(frames)

# save output
doajSearch.to_csv(os.path.join(wdtables,"KEY-DOAJ-QUERY.csv"),index=False, encoding='utf-8')
