library(dplyr)
library(tidyr)
library(igraph)
library(ggplot2)
library(ggnetwork)
library(intergraph)

by_asjc_cites = read.csv("tables/by-academic-subjects-journal-cites-(divided).csv")
by_journals_asjc_codes = read.csv("tables/by-journals-by_journals_asjc_codes-codes.csv")
journals = read.csv("data/journals-summary-scopus-extended.csv")

# join by_journals_asjc_codes major codes

by_asjc_cites = by_journals_asjc_codes %>%
  group_by(ASJC,ASJC_MAJOR) %>%
  filter(row_number(JOURNAL) == 1) %>%
  select(ASJC,ASJC_MAJOR,ASJC_MAJOR_Desc)%>%
  left_join(by_asjc_cites,., by = "ASJC")


# ---- CRERATE LINKS AND NODES TABLES ----
# journal nodes
jnodes = data.frame(
  item = journals$JOURNAL,
  item_type = "Journal",
  item_label = "", #journals$JOURNAL,
  item_counts = journals$n
)

# subject nodes
snodes = data.frame(
  item = by_asjc_cites$ASJC,
  item_type = by_asjc_cites$ASJC_MAJOR_Desc,
  item_label = by_asjc_cites$Description,
  item_counts = by_asjc_cites$cites
)

snodes$item = as.factor(snodes$item)

 # merge all nodes
nodes = jnodes %>% bind_rows(., snodes)

# ---- CREATE LINKS TABLES ----
jlinks = data.frame(
  from = by_journals_asjc_codes$JOURNAL,
  to = by_journals_asjc_codes$ASJC)

# ---- CREATE NETWORK OBJECT ----
net <- graph_from_data_frame(d=jlinks, vertices=nodes, directed=F) 

# ---- USING GGNETWORKTO PLOT IGRAHP OBJECT ----
gg = ggnetwork(net)

# useful palette
library(RColorBrewer)
n <- length(unique(gg$item_type))
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))

# sample colors
cols =sample(col_vector, n)
pie(rep(1,n), col=cols)

gg = ggplot(gg, aes(x = x, y = y, xend = xend, yend = yend)) +
  geom_edges(color = "grey50", size=0.1) +
  geom_nodes(aes(size = item_counts, color = item_type)) +
  scale_fill_manual(cols) +
  geom_nodetext(aes(label=item_label),size=2) +
  scale_size_area("",max_size = 15) +
  # geom_nodelabel(aes(label=media, color = as.factor(type.label)))
  theme_blank()

gg
ggsave("png/citations-network.png",scale = 2)
