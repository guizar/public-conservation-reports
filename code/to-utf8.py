# -----
# convert to UTF-8
# ----

import pandas as pd
from pandas import DataFrame
from io import StringIO
import unicodedata
import os
import chardet

# define working dirs
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wddata = os.path.join(proj,"data/")
wd_CSV_FIRSTITER = os.path.join(proj,"data/CSV_FIRST_ITERATION") # SOURCE
wd_CSVUTF8 = os.path.join(proj,"data/CSV-UTF8") # NEW CSVS


# ---- DEFINE FUNCTIONS----

# return encoding
def get_encoding(e):
    rawdata = open(e, "r").read()
    result = chardet.detect(rawdata)
    charenc = result['encoding']
    return charenc

# list object
LIST_CSV =['']
for file in os.listdir(wd_CSV_FIRSTITER):
    if file.endswith(".csv"):
        LIST_CSV.append(file)

LIST_CSV = LIST_CSV[1:len(LIST_CSV)]


# ---- TO UTF-8 ----
for n in range(len(LIST_CSV)) :
    # read csv and convert to utf-8
    n = LIST_CSV.index("IUCN_ELP_2009_093.csv")

    # get encoding
    enc = get_encoding(os.path.join(wd_CSV_FIRSTITER,LIST_CSV[n]))
    
    # to unicode and UTF-8 (standard python read)
    f = open(os.path.join(wd_CSV_FIRSTITER,LIST_CSV[n]))
    f = f.read()
    csvUnicode = f.decode(enc,errors='ignore')
    csvUTF8 = csvUnicode.encode('utf-8')
    
    # read_csv with encoding
    f = pd.read_csv(os.path.join(wd_CSV_FIRSTITER,LIST_CSV[n]),header=None, encoding=enc)
        # try with Windows 1252
    f = pd.read_csv(os.path.join(wd_CSV_FIRSTITER,LIST_CSV[n]),header=None, encoding="Windows 1252")
    
    
    
    # test
    print f[0]
    print csvUTF8
    

    # --- SAVE STR OBJECT TO CSV ---
    # IO handler
    sIO = StringIO(csvUnicode)

    # Convert to data frame (note above: from pandas import DataFrame)
    df = DataFrame.from_csv(sIO, sep=",", header=None, index_col=None)

    # write to csv
    df.to_csv(os.path.join(wd_CSVUTF8,LIST_CSV[n]),index=False, header=False, encoding='utf-8')
    # ---

    # OR  - SAVE PD to CSV
    f.to_csv(os.path.join(wd_CSVUTF8,LIST_CSV[n]),index=False, header=False, encoding='utf-8')
    f.to_csv(os.path.join(wd_CSVUTF8,"test.csv"),index=False, header=False, encoding='utf-8')

