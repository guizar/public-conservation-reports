library(dplyr)
library(tidyr)

# data = read.csv("data/data.csv")
journals = read.csv("data/journals-summary-scopus-extended.csv",strip.white = T,stringsAsFactors = F)

asjc = read.csv("scopus-sources-052016/ASJC-code-list.csv", strip.white = T, stringsAsFactors = F) %>% select(c(Description, Code))


# --- group AJSC CODES by major groups
lvls = seq(1000, 3600, by = 100)
lvls = lvls[-27]

asjc = 
  asjc %>%
  # group by major categories (using "cuts" by hundreds)
  group_by(gr=cut(Code, breaks= seq(1000, 3600, by = 100), right=FALSE, labels=lvls)) %>% 
  
  # create a new column to list the major acad. code, populate it with the  smallest number within each cut (presumably that's where major academic field starts)
  mutate(ASJC_MAJOR = min(Code)) %>%
  arrange(as.numeric(ASJC_MAJOR)) %>% 
  ungroup() %>%
  
  # remove "group" column
  select (.,-gr) %>%
  
  # add Major Subjects description
  left_join(.,select(asjc,c(Code,Description)), by=c("ASJC_MAJOR" = "Code"))

colnames(asjc) = c("Description","Code","ASJC_MAJOR","ASJC_MAJOR_Desc")

# ---- add subject descriptions to AJSC codes  ----
journals_asjc_long = 
journals %>%
  filter(!is.na(ASJC_CODES)) %>%
  select(JOURNAL, ASJC_CODES) %>%
  separate(ASJC_CODES,paste0("c",1:15),";") %>%
  gather("COL","ASJC",-JOURNAL) %>% 
  select(-COL) %>%
  mutate(ASJC=replace(ASJC, ASJC=="", NA)) %>%
  mutate(ASJC=as.integer(ASJC)) %>%
  filter(!is.na(ASJC)) %>% 
  left_join(.,asjc, by= c("ASJC" = "Code"))
  

# ---- get ASJC codes for journals not included in scopus and merge them to master list ----
not_in_scopus = 
  journals %>%
  filter(is.na(SCOPUS_ID)) %>%
  select(JOURNAL,SUBJECT) %>%
  left_join(.,asjc, by= c("SUBJECT" = "Description"))
  
  colnames(not_in_scopus) = c("JOURNAL","Description","ASJC","ASJC_MAJOR","ASJC_MAJOR_Desc")
  
journals_asjc_long =
    journals_asjc_long %>% bind_rows(.,not_in_scopus)

journals_asjc_long = journals_asjc_long[,c("JOURNAL","ASJC","Description","ASJC_MAJOR","ASJC_MAJOR_Desc")]

journals_asjc_long = journals_asjc_long %>%
  arrange(JOURNAL)

write.csv(journals_asjc_long,"tables/by-journals-asjc-codes.csv",row.names = F)

