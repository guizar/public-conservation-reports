library(dplyr)
library(tidyr)

journals = read.csv("data/journals-summary-scopus-extended.csv",strip.white = T,stringsAsFactors = F)

by_journals_asjc = read.csv("tables/by-journals-asjc-codes.csv", strip.white = T, stringsAsFactors = F)

# ----CALCULATE CITATION COUNTS BY SUBJECT ---
# In this process we will divide the citation counts per journal by the number of academic fields present in that journal. For each iteration (ie. journal) the result of this dividision will be allocated to each subject. Lastly we'll aggregate the sum of counts per academic field in a summary table
#

  journals %>%
  select(JOURNAL,N) %>%
  right_join(by_journals_asjc, by = "JOURNAL") %>%

  # get journal citation count / n subjects per journal
  group_by(JOURNAL) %>%
  summarise(cites_per_subject =
              unique(N) / n()) %>%

  # join by_journals_asjc table and add divided count values
  right_join(.,by_journals_asjc, by="JOURNAL") %>%
  group_by(ASJC,Description) %>%
  summarise(cites = sum(cites_per_subject)) %>%
  arrange(desc(cites)) %>%
    

  # SAVE
  write.csv(.,"tables/by-academic-subjects-journal-cites-(divided).csv",row.names = F)
