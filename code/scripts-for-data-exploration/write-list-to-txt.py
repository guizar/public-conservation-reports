# ---
# Query Crossref through API calls
# ---

import sys
import urllib
import json
import unicodedata
import os
import re
import shelve
import pickle

# --- Define WDs ----
# define home
# home = "/home/a/ag/ag448/"
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")

# define working dirs
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wddata = os.path.join(proj,"data/")

# restore lists
session = os.path.join(wddata,"refQuery1.out")
my_shelf = shelve.open(session)
for key in my_shelf:
    globals()[key]=my_shelf[key]

my_shelf.close()

# write list asa file
txtfile = os.path.join(wddata,"refQuery1.txt")

with open(txtfile, 'wb') as f:
    pickle.dump(refQuery1, f)

