# ---
# Query Crossref through API calls
# ---

import sys
import urllib
import json
import unicodedata
import os
import re
import shelve
import pickle

# --- Define WDs ----
# define home
# home = "/home/a/ag/ag448/"
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")

# define working dirs
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wddata = os.path.join(proj,"data/")

# restore lists
session = os.path.join(wddata,"refQuery1.out")
my_shelf = shelve.open(session)
for key in my_shelf:
    globals()[key]=my_shelf[key]

my_shelf.close()

n = 274

tItems = pd.DataFrame(refQuery1[n]['message']['items'])
tQuery = pd.DataFrame(refQuery1[n]['message']['query'].items())

#  ---- break down tItems ---
# tItems['container-title'] = tItems['container-title'][0]
tItems['container-title'][0][0]

#issued
# year = 
tItems['issued'][0]['date-parts'][0][0]
# month  = 
tItems['issued'][0]['date-parts'][0][1]
    
#license
tItems['license'][0]

# subject
tItems['subject']
tItems['subject'][0][0]
tItems['subject'][0][1]

# author
refQuery1[n]['message']['items'][0]['author']
tAuthor = pd.DataFrame(refQuery1[n]['message']['items'][0]['author'])
tAuthor = pd.DataFrame(refQuery1[n]['message']['items'][0]['author'])
tAffiliaiton = pd.DataFrame(refQuery1[n]['message']['items'][0]['author'][0]['affiliation'])

# title
tItems['title'][0][0]

# break down tQuery
tQuery = 
tQuery[1][1]

# link
tItems['link'][0][0]

