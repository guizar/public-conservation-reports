# -----
# convert to UTF-8
# ----

import pandas as pd
from pandas import DataFrame
from io import StringIO
import unicodedata
import os
import chardet

# define working dirs
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wddata = os.path.join(proj,"data/")
wd_CSV_FIRSTITER = os.path.join(proj,"data/CSV_FIRST_ITERATION") # SOURCE
wd_CSVWIN125 = os.path.join(proj,"data/CSV-WIN125") # NEW CSVS


# ---- DEFINE FUNCTIONS----

# return encoding
def get_encoding(e):
    rawdata = open(e, "r").read()
    result = chardet.detect(rawdata)
    charenc = result['encoding']
    return charenc

# list object
LIST_CSV =['']
for file in os.listdir(wd_CSV_FIRSTITER):
    if file.endswith(".csv"):
        LIST_CSV.append(file)

LIST_CSV = LIST_CSV[1:len(LIST_CSV)]


# ---- TO UTF-8 ----
for n in range(len(LIST_CSV)) :
    # print LIST_CSV[n]
    # read csv and convert to Windows 1252
    try:
        f = pd.read_csv(os.path.join(wd_CSV_FIRSTITER,LIST_CSV[n]),header=None, encoding="Windows 1252")
        f.to_csv(os.path.join(wd_CSVWIN125,LIST_CSV[n]),index=False, header=False, encoding='utf-8')
    except:
        print "didn't work: " + LIST_CSV[n]
    continue


