# ---
# Correct data.csv
# ---

import pandas as pd
import unicodedata
import os
import re
from StringIO import StringIO
import requests



# --- Define WDs ----
# define home
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")

# define working dirs
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wddata = os.path.join(proj,"data/")

# load data
data = pd.read_csv(os.path.join(wddata,"data.csv"),encoding = 'utf-8')


# --- Correct empty CATEGORY 1 entries, fill in missing data  ---
# filter by type and null journal
tmp = data[(data.TYPE==1) & (pd.isnull(data.JOURNAL))]
tmp.to_csv(os.path.join(wddata,"tmp.csv"),index=None,encoding="utf-8")


# create lists of missing vals for each column
journal = ["Nature","Tropical Animal Health Production","Tropical Animal Health Production","Wildlife Society Bulletin","Micronesia"]
yr = ["1995","1972","1972","2004","2001"]
mthd = [1,1,1,1,1]

# replace filtering by index values included in tmp file
data.ix[tmp.index.values, 'JOURNAL'] = journal 
data.ix[tmp.index.values, 'YEAR'] = yr
data.ix[tmp.index.values, 'METHOD'] = mthd
data.ix[tmp.index.values, 'CERTAINTY']  = mthd

# verify results
data[(data.TYPE==1) & (pd.isnull(data.JOURNAL))] # nothing
data.ix[tmp.index.values] # these rows have values as expected


# ---- REPLACE false positives ['TYPE']==1 ----
# read npr table
npr = pd.read_csv(os.path.join(wdtables,"npr-replacements.csv"),encoding = 'utf-8')

for e in range(len(npr)) :
    # get ixs
    tmp = data[data.JOURNAL==npr.JOURNAL_ENTRY[e]]
    # replace values
    data.ix[tmp.index.values, 'TYPE'] = 3 


# ---- REPLACE Category values of ['JOURNAL']==NPR  ----
idx = data.JOURNAL.str.contains('^NPR: ',flags=re.IGNORECASE, regex=True, na=False)
tmp = data[idx]
tmp.to_csv(os.path.join(wddata,"tmp.csv"),index=None,encoding="utf-8")

# replace values
data.ix[tmp.index.values, 'TYPE'] = 3 


# ---- REPLACE INCORRECT JOURNAL NAMES
# load
revised = pd.read_csv(os.path.join(wdtables,"corrected-journal-names.csv"),encoding = 'utf-8')
# change values

for e in range(len(revised)) :
    # get ixs
    tmp = data[data.JOURNAL==revised.journal[e]]
    # replace values
    data.ix[tmp.index.values, 'JOURNAL'] = revised.revised_name[e]



# ---- REPLACE REVISED JOURNAL NAMES
# first run journal-names-partial-match and review the table
# save the results as REVISED-journal-names-nomatch... and load 

# load
revised = pd.read_csv(os.path.join(wdtables,"REVISED-journal-names-nomatch-20160403.csv"),encoding = 'utf-8')

# select changes
revised = revised[revised.change=="T"]

# change values
for e in range(len(revised)) :
    # get ixs
    tmp = data[data.JOURNAL==revised.journal[e]]
    # replace values
    data.ix[tmp.index.values, 'JOURNAL'] = revised.revised_journal_name[e]

# ---- SAVE DATA OBJECTS----

# DATA
data.to_csv(os.path.join(wddata,"data.csv"),index=False, header=True, encoding="utf-8")

# PR - references and reports (for post analysis via crossref's api) 
PR = data[data.TYPE==1]
PR = PR.loc[:,["REFERENCE","REPORT"]]
PR.to_csv(os.path.join(wddata,"PR.csv"), header=True, encoding="utf-8",index=False)
