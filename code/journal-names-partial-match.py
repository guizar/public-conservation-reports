    
# ---
# Compare journal names against their revised versions (gsheet)
# ---

import pandas as pd
import unicodedata
import os
import re
from StringIO import StringIO
import requests
from time import gmtime, strftime


# --- Define WDs ----
# define home
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")

# define working dirs
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wddata = os.path.join(proj,"data/")

# load data
data = pd.read_csv(os.path.join(wddata,"data.csv"),encoding = 'utf-8')
revised_names = pd.read_csv(os.path.join(wdtables,"KEY-SUBJECT-ISSN.csv"))
# url = requests.get('https://docs.google.com/spreadsheets/d/1Ksaz2sPfEsbrhJNkNhZwmnTeQUz5NhrN9JdlZNagN4o/pub?gid=950686145&single=true&output=csv')
# revised_names = url.content
# revised_names = pd.read_csv(StringIO(revised_names))



# --- FUN: pandas df with unique column counts
def unique_counts(data,column):
    '''
    Takes a pandas DF column and performS value_counts()
    Returns a pandas DF with unique value counts

    -- INPUTS --
    data: Pandas DF
    column: (str) column name
    '''
    df =  pd.DataFrame(data[column].value_counts())
    df[column.lower()] = df.index # pass index as columns
    df = df.reset_index(drop=True)
    cols = df.columns.values
    cols[0]="count"
    df.columns  = cols
    return df

# --- FUN: Get substrings ---
def get_sub_strings(string): # http://stackoverflow.com/questions/27182912/python-check-for-partial-match-of-strings-between-two-lists
    '''
    Splits words and concatenates them by incrementing one word per iteration
    '''
    words = string.split()
    for i in xrange(len(words)+1, 0, -1):
        for n in xrange(0, len(words)+1-i):
            yield ' '.join(words[n:n+i])

# --- FUN: which is NOT matching? pandas df rowise match ---
def cols_not_match(df,column1,column2):
    return df[df[column1] != df[column2]]

# --- FUN: concat str with TM (to name CSVS)---
def str_tm(string):
    tm = strftime("%Y%m%d", gmtime())
    strtm = string + "-" +  tm
    return strtm



# csvname = str_tm("journal-names-revised") + ".csv"
# revised_names.to_csv(os.path.join(wdtables,csvname))



# --- FUN: Find partial matches ---
def find_partial_match(stringList1,stringList2,names,threshold):
    '''
    Performs a many-to-many comparison between lists elements
    If >= the threshold condition, returns the largest string match sequence between list1[e] and list2[e]

    --- INPUTS ---
    stringList1: list of strings
    stringList2: list of strings to compare against stringList1
    names: list of 2 str elements which will be used to name the columns of the returned df
    threshold: minimum "matching percentage" between list1[e] and list2[e]

    returns df with 3 cols: str1  | str2 | string matches
    '''
    # Define matching lists
    lsStr1= []
    lsStr2= []
    lsOuts= []

    # Iterate over input lists and append outer lists if matches occour
    for str1 in stringList1:
        for str2 in stringList2:
                    out=[]
                    str1splt = str1.split()
                    str2splt = str2.split()

                    for word in str1splt:
                        for sub in get_sub_strings(word.lower()):
                            if any(sub in s.lower() for s in str2splt):
                                    out.append(sub)
                                    break

                    if len(str1splt) >= len(str2splt):
                        thresh = float(len(out)) / float(len(str1splt))
                    else:
                        thresh = float(len(out)) / float(len(str2splt))

                    if thresh >=  threshold:
                        lsStr1.append(str1)
                        lsStr2.append(str2)
                        lsOuts.append(out)

    # Build Pandas DF and return
    df = pd.DataFrame({names[0]:lsStr1, names[1]:lsStr2, "match":lsOuts})
    return df



# filter to TYPE==1
data = data[data.TYPE==1]



# Count unique journal names
unique = unique_counts(data,"JOURNAL")



# Match lists: journal names vs. revised journal names
matched = find_partial_match(pd.Series.tolist(unique.journal),
                                    pd.Series.tolist(revised_names.JOURNAL),
                                    ["journal","revised_journal_name"],
                                    .75)

# From matched, remove identical matches: use this table to supervise row by row if the remaining
# matches make reference to the same element
nomatch = cols_not_match(matched,"journal","revised_journal_name")



# Save matching results to have a closer look offline
csvname = str_tm("journal-names-compared") + ".csv"
matched.to_csv(os.path.join(wdtables,csvname),index=False)

csvname = str_tm("journal-names-nomatch") + ".csv"
nomatch.to_csv(os.path.join(wdtables,csvname),index=False)



# --- QUERY BOX: use it to retrive the bibliography data of a given journal ----
regquery = '(^Acta Zoologica)'

# query is returned as Pandas.core.Series
# q = data.JOURNAL.str.contains(regquery)
# q = data.REFERENCE.str.contains(regquery)

# to use q object, exclude NaNs with .fillna
# for e in data.REFERENCE[q.fillna(False)]:
#     print e

# data[q.fillna(False)]
