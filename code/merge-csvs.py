# ----- CSV MERGE ------
# Merge all csvs into one

import pandas as pd
import unicodedata
import os
import csv

# --- Define WDs ----
# define home
home = "/Users/alejandroguizar/"
proj = os.path.join(home,"Py-R/conservation-reports/")

# define working dirs
wdpng = os.path.join(proj,"png/")
wdtables = os.path.join(proj,"tables/")
wd_CSV = os.path.join(proj,"data/CSV-UTF8")
wddata = os.path.join(proj,"data/")


# ---- LIST CSVS ----
LIST_CSV =['']
for file in os.listdir(wd_CSV):
    if file.endswith(".csv"):
        LIST_CSV.append(file)

# remove first empty item
LIST_CSV = LIST_CSV[1:len(LIST_CSV)]

# ----  FUNs (PANDAS WAY, with the caveat of not having int cols)  ----
def csv_read(e):
    f = pd.read_csv(os.path.join(wd_CSV,LIST_CSV[e]),header=None)
    return f

def csv_merge(e):
    global data
    f1 = csv_read(e)
    f1[6] = LIST_CSV[e][0:17] # add report name
    data = data.append(f1)
    return data
# ---

# --- RUN FUNCTIONS ---
columns = range(7)
data = pd.DataFrame(columns = columns)

for e in range(len(LIST_CSV)) :
    print LIST_CSV[e]
    csv_merge(e)

# Clean
data = data.drop(data.columns[[7,8,9,10,11,12]], axis=1)
data.columns = ["REFERENCE","TYPE","JOURNAL","YEAR","METHOD","CERTAINTY","REPORT"]

# Remove white
data.REFERENCE = data.REFERENCE.str.strip()
# data.TYPE = data.TYPE.str.strip()
data.JOURNAL = data.JOURNAL.str.strip()
# data.YEAR = data.YEAR.str.strip()
# data.METHOD = data.METHOD.str.strip()
# data.CERTAINTY = data.CERTAINTY.str.strip()



# data.describe()


# save file
data.to_csv(os.path.join(wddata,"data.csv"),index=False, header=True, encoding="utf-8")

