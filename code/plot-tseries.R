#----
# Time-series of citations rates
# ----

library(dplyr)
library(tidyr)
library(ggplot2)

# ---- load
data = read.csv(file = "data/data.csv",header = T, 
stringsAsFactors = F, strip.white = T)
data = tbl_df(data)

journals = read.csv(file = "data/journals-summary-scopus-extended.csv",header = T, stringsAsFactors = F, strip.white = T) 

# --- add tags (colums) in data for reporting
data$REPORT_ID =  substr(data$REPORT,0,8)
data$REPORT_YR =  as.numeric(substr(data$REPORT,10,13))
data[is.na(data$TYPE),"TYPE"] = 3 # CHANGE NANS TO 3

# --- ANALYSE OPEN ACCESS ----
# list DOAJ as OA
journals$OA[which(journals$OPEN_ACCES_STATUS == "DOAJ/ROAD Open Access")] = TRUE

# change TYPE to 4 for OA references
oa = journals %>%
filter(OA==T)

# if there is an OA_year value: it determines whether or not the reference was OA depending on the date when the report was produced and the year when the journal in question became OA
for( e in seq(length(oa$JOURNAL))){
  ix = which(data$JOURNAL %in% oa$JOURNAL[e])
  for(i in ix){
    if(data$YEAR[i]>= oa$OA_YR[1]){
      data$TYPE[i]=4
      }}}

# --- Summary tables for plotting----
data %>%
group_by(TYPE)%>%
tally() %>%
summarise(n = sum(n))

by_year = 
data %>%
group_by(REPORT_YR,TYPE, REPORT_ID) %>%
tally() 


# Reports by year, bin it as categories
by_year_reports = 
data %>%
select(REPORT_YR,REPORT) %>%
group_by(REPORT_YR)%>%
summarise(count = n_distinct(REPORT)) %>%
group_by(by_year_reports=cut(count, breaks = 5,dig.lab = 0)) 
# change levels
levels(by_year_reports$by_year_reports) = c("1 - 5", "6 - 10", "11 - 14", "15 - 19", "20 - 25")


# join PR counts, report counts by year, and 0A counts
by_year_PR = 
by_year %>% filter(TYPE==1 | TYPE ==4) %>% ungroup(.) %>%
group_by(REPORT_YR) %>% 
summarise(n = sum(n))

oa_counts = by_year %>% filter(TYPE ==4) %>% ungroup(.) %>%
group_by(REPORT_YR) %>% 
summarise(OA = sum(n))

# join PR and OA
by_year_PR = by_year_PR %>% left_join(.,oa_counts,by ="REPORT_YR") 
# join PR and report counts
by_year_PR = by_year_PR %>% left_join(.,by_year_reports,by ="REPORT_YR") 

# --- EXTENED by_year TABLE for further analysis (PR VS NPR) ---

# change types to PR and NPR
library(plyr)
by_year$TYPE = as.factor(by_year$TYPE)
by_year$TYPE = revalue(by_year$TYPE, c("1"="Peer-review", 
                        "2"="Non peer-review",
                        "3"="Non peer-review",
                        "4"="Peer-review"))
detach("package:plyr", unload=TRUE)

# SUMMARISE
by_year = 
  by_year %>% 
  select(.,-REPORT_ID) %>% 
  group_by(REPORT_YR,TYPE) %>%
  summarise(n = sum(n))

# add years as POSIXct
by_year$DATE = strptime(by_year$REPORT_YR,"%Y")
by_year$DATE = as.POSIXct(by_year$DATE)

# divie citations / reports published / year
by_year = by_year %>% 
  left_join(.,by_year_reports %>% ungroup(.) %>% 
              select(.,-by_year_reports), by = "REPORT_YR") %>% 
              mutate(RATE_YEAR = n/count)

colnames(by_year)[5] = "REPORTS_PUBLISHED"

by_report_year = by_year %>% 
  select(REPORT_YR,TYPE,RATE_YEAR) %>% 
  spread(.,TYPE,RATE_YEAR,fill = 0) %>%
  mutate(TOTAL = `Peer-review` + `Non peer-review`) %>%
  mutate(PR_PERCENT = `Peer-review` / TOTAL)

# ---- PLOT TIMESERIES----
# -- PR, OA and reports publisged by year --
gg = ggplot(by_year_PR, 
            aes(x = REPORT_YR, y = n, 
                group=REPORT_YR, fill=by_year_reports))
gg = gg + geom_bar(stat="identity", width= 0.5, colour="black")
gg = gg + theme_bw()
gg = gg + scale_fill_grey(start = .9, end = 0,name="Published \n reports") 
# + scale_colour_grey(start = .9, end = 0)
gg =  gg  + labs(title = "Peer-review and Open Access through time", x = "Year", y = "Peer-review counts")
gg = gg  + geom_text(aes(label=OA), position=position_dodge(width=0.9), vjust=-0.25)
gg

tseriesPR = gg

ggsave("png/pr-oa-timesieries.png",tseriesPR,scale = 1.5)

# --- PR VS NPR through time
gg = ggplot(by_year, aes(x = DATE, y = n, fill=TYPE,linetype=TYPE))
gg = gg + geom_area()
gg = gg + theme_bw()
gg = gg + geom_smooth(method = lm,se=FALSE) 
gg = gg  + labs(title = "Bibliographic sources through time", x = "Year", y = "Citation counts")
# gg + scale_fill_grey(start = 0.1, end = 0.8,name="Reference \n type")  // messes with the lables
tseriesVS = gg
tseriesVS
ggsave("png/pr-vs-npr-timesieries.png",tseriesVS, scale = 1.5)


# --- trend: PR citations / year / reports published
# 
gg = ggplot(by_year,
            aes(x = REPORT_YR, y = RATE_YEAR, fill = TYPE, linetype=TYPE))
gg = gg + geom_smooth()
gg = gg + theme_bw()
# gg = gg + scale_fill_grey(start = 0.1, end = 0.8,name="") 
gg =  gg  + labs(title = "Trends by citation type", x = "Year", y = "Citations per published report")
# gg = gg + geom_area(data = subset(by_year, TYPE =="Peer-review"),
               # aes(REPORT_YR, REPORTS_PUBLISHED))

tseriesPR_trend = gg

ggsave("png/citations-reports-year-trend.png",tseriesPR_trend, scale = 1.5)

# ----- #



 

# ALL reference counts thourgh time
gg = ggplot(by_year, aes(x = REPORT_YR, y = n, group=REPORT_YR,  fill=as.factor(TYPE), color=as.factor(TYPE)))
gg = gg + geom_bar(stat="identity")
gg = gg + theme_bw()
gg =  gg  + labs(title = "References through time", 
        x = "Year", y = "Reference counts")
gg = gg + scale_fill_grey(start = 0, end = .9) + scale_colour_grey(start = 0, end = .9)
tseriesAll = gg


# OA only
gg = ggplot(by_year %>% filter(TYPE ==4), 
            aes(x = REPORT_YR, y = n, 
                group=REPORT_YR, fill=as.factor(TYPE), 
                color = as.factor(TYPE)))
gg = gg + geom_bar(stat="identity", width= 0.5)
gg = gg + theme_classic()
gg =  gg  + labs(title = "Open Access through time", x = "Year", y = "Reference counts")
tseriesOA = gg

