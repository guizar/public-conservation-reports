# define variables
date=$(date +"%Y%m%d")
time=$(date +"%T")
filename="join-all-csvs"
nohup2=$date"_"$time"_"$filename".out"
rscript=$filename".R"

# move nohup
mv nohup2.out ~/Py-R/conservation-reports/shell-messages/$nohup2

# load modules
module load python
module load R

# RUN
# Conservation-reports/code/
# Rscript ~/R/Conservation-reports/code/$rscript


exit
